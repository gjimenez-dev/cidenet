'use strict';

function createEmailEmployee(name, surname, domain, init ) {
    const email = name.replace(/\s+/g, '').toLowerCase() + '.' + surname.replace(/\s+/g, '').toLowerCase()
    + (init > 0 ? '_' + init + '@' : '@') + domain;
    return email;
}


module.exports = { createEmailEmployee };